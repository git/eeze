= eeze
apiote <me@apiote.xyz>
v2.0.3 2022-04-12
:toc:

eeze is a simple secret storage/password manager

WARNING: eeze has not been audited and is by no means guaranteed to be safe

== Building

To build eeze, You need:

* `go>=1.17`
* `mk`

Then, all You have to do is run `mk`, and—optionally—`mk install`

== Running

----
eeze -A
  add secret(s);

  reads a
  []map[string]struct{
    Hidden bool
    Value string
  }
  from stdin as dirty
eeze -D -i <id>
  delete a secret by <id>
eeze -E
  export all secrets as dirty
eeze -G [-e|-p] [-f <fields>] (-i <id>|-l <label> [-u <username>]|-s <url> [-u <username>])
  get secrets

  -e             export as dirty
  -p             pretty print
  -f <fields>    get only fields for secrets; fields are comma separated
  -i <id>        get secret by id field
  -l <label>     get secret by label field
  -s <url>       get secret by url field
  -u <username>  filter secrets by username field
eeze -L
  list all secrets in format "id|label" or "id|label: username"
eeze -Ik
  list credentials, name and type
eeze -Ir
  recreate secrets index
eeze -Ia -n <name> (-p|-2)
  add credential with <name>

  -p             password credential
  -2             FIDO2 credential
eeze -Id -n <name>
  delete credential named <name>
eeze -h
  show this help and exit
----

usage, examples, helpers

== Contribute

This project is finished; no more functions will be implemented; all feature requests will be ignored.

This project uses The Code of Merit, which is available as CODE_OF_CONDUCT file.

Fixes and patches are welcome; please send them to `eeze@git.apiote.xyz` using `git send-email`. They must include a sign-off to certify agreement to https://developercertificate.org/[Developer Certificate of Origin].

All communication—questions, bugs, etc.—should go through the mailing list available at `eeze@git.apiote.xyz`. Note that all communication will be made public at https://asgard.apiote.xyz/.

== Mirrors

The canonical repository for this project is https://git.apiote.xyz/eeze.git it’s mirrored at https://notabug.org/apiote/eeze

Mirrors exist solely for the sake of the code and any additional functions provided by third-party services (including but not limited to issues and pull requests) will not be used and will be ignored.

== License

----
eeze Copyright (C) 2022 apiote

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
----
