package fs

import (
	"os"
)

func DataHome() string {
	xdgDataHome, _ := os.LookupEnv("XDG_DATA_HOME")
	if xdgDataHome == "" {
		user, err := os.UserHomeDir()
		if err != nil {
			panic(err)
		}
		xdgDataHome = user + "/.local/share"
	}
	return xdgDataHome + "/eeze"
}

