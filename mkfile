all:V: eeze

eeze: main.go `echo crypto/*.go fido/*.go fs/*.go secrets/*.go`
	go build

PREFIX=`echo ${PREFIX:-/usr/local}`
install:V: eeze
	mkdir -m755 -p $PREFIX/bin
	cp eeze $PREFIX/bin/eeze
	chmod 755 $PREFIX/bin/eeze
	install eeze.sh $PREFIX/bin/eeze.sh
	chmod 755 $PREFIX/bin/eeze.sh
	cp -r eeze.sh.d $PREFIX/bin
	ln -fs eeze.sh $PREFIX/bin/eeze-add
	ln -fs eeze.sh $PREFIX/bin/eeze-del
	ln -fs eeze.sh $PREFIX/bin/eeze-edit
	ln -fs eeze.sh $PREFIX/bin/eeze-find

uninstall:V:
	rm $PREFIX/bin/eeze
	rm $PREFIX/bin/eeze.sh
	rm $PREFIX/bin/eeze-add
	rm $PREFIX/bin/eeze-del
	rm $PREFIX/bin/eeze-edit
	rm $PREFIX/bin/eeze-find
	rm -r $PREFIX/bin/eeze.sh.d
