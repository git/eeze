module apiote.xyz/p/eeze

go 1.17

require (
	apiote.xyz/p/go-dirty v0.0.0-20211218161334-e486e7b5cf43
	git.sr.ht/~sircmpwn/getopt v1.0.0
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
	github.com/keys-pub/go-libfido2 v1.5.2
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)

require (
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
