#!/bin/sh

set -e

script=$(basename "$0")

case "$script" in
	eeze-init)
		. "$(dirname "$0")/eeze.sh.d/init.sh"
		eeze_init "$@"
		;;
	eeze-find)
		. "$(dirname "$0")/eeze.sh.d/find.sh"
		eeze_find "$@"
		;;
	eeze-edit)
		. "$(dirname "$0")/eeze.sh.d/edit.sh"
		eeze_edit "$@"
		;;
	eeze-add)
		. "$(dirname "$0")/eeze.sh.d/add.sh"
		eeze_add "$@"
		;;
	eeze-del)
		. "$(dirname "$0")/eeze.sh.d/del.sh"
		eeze_del "$@"
		;;
esac
