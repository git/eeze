//go:build nofido

package fido

import (
	"fmt"
)

type Device struct {
	Info struct {
		AAGUID []byte
	}
}

func Setup(rpID, pin string, cdh []byte, device Device) ([]byte, error) {
	return []byte{}, fmt.Errorf("Compiled without FIDO2 support")
}

func GetHmacSecret(rpID, pin string, cdh, credentialID, salt []byte, device Device) ([]byte, error) {
	return []byte{}, fmt.Errorf("Compiled without FIDO2 support")
}

func GetPluggedDevices() ([]Device, error) {
	return []Device{}, fmt.Errorf("Compiled without FIDO2 support")
}

func GetUUID() ([]byte, error) {
	return []byte{}, fmt.Errorf("Compiled without FIDO2 support")
}
