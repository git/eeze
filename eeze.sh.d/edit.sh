#!/bin/sh

eeze_edit() {
	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze || exit 1
		printf "pulling changes from remote repository\n"
		git pull
	fi

	secret=$(eeze -L | fzf -d '\|' --with-nth 2..)
	id=$(echo "$secret" | cut -d '|' -f1)
	eeze -Gei "$id" > /tmp/eeze-edit.dirty
	$EDITOR /tmp/eeze-edit.dirty
	eeze -A </tmp/eeze-edit.dirty
	rm /tmp/eeze-edit.dirty

	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze/ || exit 1
		git add "secrets/$id"
		git add index.bare
		git commit -m "edit secret $id"
		printf "pushing changes to remote repository\n"
		git push
	fi
}

