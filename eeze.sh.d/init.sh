#!/bin/sh

eeze_init() {
	eeze >/dev/null 2>&1

	cd ~/.local/share/eeze || exit 1
	git init
}
