#!/bin/sh

eeze_find() {
	secrets=$(eeze -L)
	secret=$(echo "$secrets" | fzf -d '\|' --with-nth 2..)
	id=$(echo "$secret" | cut -d '|' -f1)
	eeze -Gi "$id" "$@"
}
