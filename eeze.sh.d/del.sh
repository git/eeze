#!/bin/sh

eeze_del() {
	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze || exit 1
		printf "pulling changes from remote repository\n"
		git pull
	fi

	secret=$(eeze -L | fzf -d '\|' --with-nth 2..)
	id=$(echo "$secret" | cut -d '|' -f1)
	eeze -Di "$id"

	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze/ || exit 1
		git rm "secrets/$id"
		git add index.bare
		git commit -m "delete secret $id"
		printf "pushing changes to remote repository\n"
		git push
	fi
}
