#!/bin/sh

eeze_add() {
	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze || exit 1
		printf "pulling changes from remote repository\n"
		git pull
	fi

	printf "(\n\t(\n\t\t('label' (('value' '') ('hidden' false)))\n\t\t('username' (('value' '') ('hidden' false)))\n\t\t('url' (('value' '') ('hidden' false)))\n\t\t('password' (('value' '') ('hidden' true)))\n\t)\n)" > /tmp/eeze-new-password

	$EDITOR /tmp/eeze-new-password
	eeze -A </tmp/eeze-new-password
	command -v shred && shred -z /tmp/eeze-new-password
	rm /tmp/eeze-new-password

	if git status >/dev/null 2>&1
	then
		cd ~/.local/share/eeze || exit 1
		git add secrets/
		git add index.bare
		git commit -m 'add secret'
		printf "pushing changes to remote repository\n"
		git push
	fi
}
